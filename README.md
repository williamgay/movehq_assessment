<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## FOR THIS ASSESSMENT, I USED LARAVEL AND VUE.JS

To get the application running, first clone this repo to your local. I use Valet so the exact configuration could be slightly different if you use something different.

After cloning:

- CD into the directory
- Copy .env.example to .env and make any needed changes in regards to database
- Run composer install
- Run NPM install
- run php artisan key:generate
- run php artisan migrate --seed  ( By running seed, a user will be created with the email address test@test.com and password of... password)
-run npm run dev

At this point, open the application in a browser and sign in with the above credentials or register and create a new user. Once logged in, the dashboard should the "Companies" component. Click Create company to add a new company. You will be presented with a form that asks for the name, email address, address and website of the company. All inputs do use validation. Once the form is filled out, click Create and the company is added.
You can edit or delete the company if needed.



## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
